# Do NOT accept environment variables.
AcceptEnv no

# Use both the IPv4 and IPv6 address families.
AddressFamily any

# Do not allow agent forwardig.
# According to the man page, the shell should also be disabled here to have an effect on security, however, it is being disabled as it means there is one less thing to have to configure.
AllowAgentForwarding no

# Groups which are allowed to connect via SSH.
AllowGroups ssh-pubauth ssh-passauth

# Do NOT allow TCP forwarding by default.
AllowTcpForwarding no

# Disable ChallengeResponseAuthentication
ChallengeResponseAuthentication no

# Only use the AES 256 CBC cipher.
# Selecting a single cipher makes managing downgrade attacks easier, since you only need to worry about one type of cipher.
# Ideally, users would set their accepted cipbers to this one, so that in the event that an attacker tried to do a MitM attack, they would also need to match the cipher, which makes the attacks job slightly harder, and easier to be detected.
# Of course, if the attacker knows what cipher is expected, then this won't do anything.
Ciphers aes256-cbc

# Send five ClientAlive messages (encrypted anti-timeout protection essentially) without a response from the client before dropping the connection.
ClientAliveCountMax 5

# Send one ClientAlive message per minute.
ClientAliveInterval 60

# Compress any following transmission once the user has authenticated.
Compression delayed

# Do NOT allow non-client users to connect to ports forwarded by the client.
GatewayPorts no

# Disable GSSAPI authentication.
GSSAPIAuthentication no

# Disable host-based authentication.
HostbasedAuthentication no

# The private key which this host/server will use.
HostKey /etc/ssh/ssh_host_ed25519_key

# Explicitly disable keyboard interactive authentication.
# This setting is implicitly changed by the state of GSAPI authentication, which is disabled anyway.
KbdInteractiveAuthentication no

# Disable Kerberos authentication.
KerberosAuthentication no

# Listen for incoming connections on port 22.
Port 22

# The address for SSH to listen on.
ListenAddress 0.0.0.0
ListenAddress ::

# Disconnect any users which have not authenticated within 120 seconds.
# Keep bad connections in mind when setting this.
LoginGraceTime 60

# Use verbose logging.
LogLevel VERBOSE

# Use the 'hmac-sha2-256-etm@openssh.com' MAC.
# This is used for data integrity, but having only one means you only have to worry about one using bad code.
MACs hmac-sha2-256-etm@openssh.com


# Allow 1 attempt to login in a single session before kicking (not banning) the user.
# This forces bad logins immediately (well, it should).
MaxAuthTries 10

# Allow 5 sessions to be open from any network connection.
MaxSessions 5

# Allow 5 un-authenticated SSH sessions at any given time.
# This is for the entire server.
MaxStartups 5

# Password authentication is allowed on a per-group basis.
# Disable by default.
PasswordAuthentication no

# Do not allow empty passwords.
PermitEmptyPasswords no

# Do NOT permit root login.
PermitRootLogin no

# Do not allow tunnels (tun0).
PermitTunnel no

# Do not allow PTYs by default.
PermitTTY no

# Do not ready environment information from the AuthorizedKeys file or 'environment' file in the users '.ssh' folder.
PermitUserEnvironment no

# Do not display information about the last user that logged in when a user logs in.
PrintLastLog no

# Do not print the MOTD.
PrintMotd no

# Use SSH protocol version 2.
Protocol 2

# Disable public key authentication by default.
# This is enabled on a per-user basis.
PubkeyAuthentication no

# Maximum amount of data (in this configuration - time can be specified in addition to data) which can be transmitted before negociating a new ephemercal key for the session.
RekeyLimit 1G

# Check the home folder of the user which is logging in to ensure all of the necessary file permissions are correct.
StrictModes yes

# Use the built-in SFTP program for SFTP.
Subsystem sftp internal-sftp

# Use the AUTH Syslog facility.
# The log file for SSH should end up in /var/log/auth.log.
SyslogFacility AUTH

# Do not use TCP keep-alive messages.
# Time-outs are handled with a different method configured above which requires the encryption to be broken in order to be spoofed.
TCPKeepAlive no

# Do not attempt to resolve the remote host name.
UseDNS no

# Use the Pluggable Authentication Module (PAM) for authentication.
UsePAM yes

# Use an unprivileged user for pre-authentication.
UsePrivilegeSeparation yes

# Do not provide the SSH server version to incoming connections.
VersionAddendum none

# Do not allow X11 forwarding.
X11Forwarding no
