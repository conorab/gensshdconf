# GenSSHDConf

Script made in 2017-2018 that generates an SSHD configuration file from two base configurations - one generalised one intended for all machines and one machine-specific configuration. This script is no longer maintained.
